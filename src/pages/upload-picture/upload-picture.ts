import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams,
  LoadingController, ToastController, AlertController } from 'ionic-angular';

import { TellUsPage } from '../tell-us/tell-us';
import { ProfilePage } from '../profile/profile';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

import { User, UserDetail, userPicture } from "../interfaces/datingscript";
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the UploadPicturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upload-picture',
  templateUrl: 'upload-picture.html',
})
export class UploadPicturePage {
  imgurl: any;
  myphoto: any;
  nativepath: any;
  imageURI:any = '';
  imageFileName:any;
  base64Image: string;
  formData = new FormData();
  userID    : any  = '';
  currentImage = null;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public filechooser: FileChooser,
              private transfer: FileTransfer,
              public loadingCtrl: LoadingController,
              public toastCtrl: ToastController,
              private camera: Camera,
              public http: Http,
              public alertCtrl: AlertController,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.imageURI = '';
    this.userID = '';
    if(localStorage.getItem('user')) {
      let user = JSON.parse(localStorage.getItem('user'));
      this.userID = user.user_id;
    }
    else {
      this.goto('S');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadPicturePage');
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false


      // destinationType: this.camera.PictureSourceType.FILE_URI,
      // sourceType: this.camera.PictureSourceType.PHOTOLIBRARY

      // destinationType: navigator.camera.PictureSourceType.FILE_URI,
      // sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
      // mediaType: navigator.camera.MediaType.ALLMEDIA
    }

    this.camera.getPicture(options).then((imageData) => {
      this.currentImage = imageData;
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      // this.myphoto = 'data:image/jpeg;base64,' + imageData;
      // console.log('this.myphoto ' + this.myphoto);
    }, (err) => {
      // Handle error
      console.log('image error ' + err);
    });
  }

  uploadImage(){
    //Show loading
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    //create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();

    //random int
    var random = Math.floor(Math.random() * 100);

    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: "myImage_" + random + ".jpeg",
      chunkedMode: false,
      httpMethod: 'POST',
      mimeType: "image/jpeg",
      headers: {}
    }

    let params = {
      image: this.currentImage
    };
    options.params = params;

    // var formData: FormData = new FormData();
    // formData.append('image', this.myphoto);
    //file transfer action
    fileTransfer.upload(this.currentImage, 'localhost/uploadPic/46', options)
      .then((data) => {
        alert("Success");
        loader.dismiss();
      }, (err) => {
        console.log(JSON.stringify(err));
        alert("Error");
        loader.dismiss();
      });
  }



  uploadAPI() {
    this.filechooser.open().then((url) => {
      console.log('url : ' + url);
      this.imageURI = url;
    });
  }

  uploadFile() {
      // if(this.imageURI !== '') {
        this.filechooser.open().then((url) => {
          console.log('url : ' + url);
          this.imageURI = url;
          (<any>window).FilePath.resolveNativePath(this.imageURI, (result) => {
            console.log('result : ' + result);
            this.nativepath = result;
            console.log('this.nativepath : ' + this.nativepath);
            // this.imageURI = this.nativepath;
            (<any>window).resolveLocalFileSystemURL(this.nativepath, (res) => {
              res.file((resFile: File) => {
                // let u = resFile.localURL;
                // let filename = u.substr(u.lastIndexOf('/') + 1);
                // console.log('filename  ' + filename);

                // var formData: FormData = new FormData();
                // formData.append('image', resFile, filename);
                // this.http.post('localhost/uploadPic/46', formData)
                //   .subscribe(response => {console.log('response : ' + response)},
                //     error2 => {
                //       console.log('ERROR : ' + JSON.stringify(error2));
                //     });


                // let loader = this.loadingCtrl.create({
                //   content: "Uploading..."
                // });
                // loader.present();
                // const fileTransfer: FileTransferObject = this.transfer.create();
                //
                // let options: FileUploadOptions = {
                //   fileKey: 'ionicfile',
                //   fileName: filename,
                //   chunkedMode: false,
                //   headers: {}
                // }

                // let params = {
                //   image: filename
                // };
                // options.params = params;

                // let user_picture: userPicture = {} as any;
                // user_picture.image = this.base64Image;
                // console.log(u);
                // fileTransfer.upload(url, 'localhost/uploadPic/46', options)
                //   .then((data) => {
                //     console.log(data + " Uploaded Successfully");
                //     // this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
                //     loader.dismiss();
                //     this.presentToast("Image uploaded successfully");
                //   }, (err) => {
                //     console.log(JSON.stringify(err));
                //     loader.dismiss();
                //     this.presentToast(err);
                //   });
              });
            });
          });
        });
      // }
      // else {
      //   this.presentAlert('Please select an image first');
      // }
  }
  //
  // openCamera() {
  //   const options: CameraOptions = {
  //     quality: 100,
  //     destinationType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE
  //   }
  //
  //   this.camera.getPicture(options).then((imageData) => {
  //     this.base64Image = 'data:image/jpeg;base64,' + imageData;
  //   }, (err) => {
  //     this.presentToast(err);
  //   });
  // }

  openGallery() {
    // this.filechooser.open().then((url) => {
    //   console.log('url : ' + url);
    //   this.imageURI = url;
    // });

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    // this.camera.getPicture(options).then((imageData) => {
    //   console.log('imageData ' + imageData);
    //
    //   this.formData.append('image',imageData);
    //   this.base64Image = 'data:image/jpeg;base64,' + imageData;
    // }, (err) => {
    //   this.presentToast(err);
    // });

    this.camera.getPicture(options).then((imageData) => {
      // this.imageURI =  this.sanitizer.bypassSecurityTrustUrl("data:image/jpeg;base64," + imageData)
      // console.log('imageURI ' + this.imageURI + ' imageData ' );
      // // const imageFile = new File([imageData], 'myImage.jpg', { type: 'image/jpeg' })



      const fileName = imageData.split('/').pop();
        const path = imageData.replace(fileName, '');
        console.log('File fileName and path : ', fileName + ' ' + path);
      this.imageURI = imageData;
      (<any>window).FilePath.resolveNativePath(this.imageURI, (result) => {
        console.log('result : ' + result);
        this.nativepath = result;
        console.log('this.nativepath : ' + this.nativepath);
        // this.imageURI = this.nativepath;
        (<any>window).resolveLocalFileSystemURL(this.nativepath, (res) => {
          console.log('res : ' + res);
          // this.loadFile(res);
          res.file((resFile) => {
            console.log('resFile : ' + resFile);
            var reader = new FileReader();
            reader.readAsArrayBuffer(resFile);
            reader.onloadend = (evt: any) => {

              console.log('evt : ' + JSON.stringify(evt.target.result));
            }
              // var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
              //
              // this.loadFile();
          //   let u = resFile.localURL;
          //   let filename = u.substr(u.lastIndexOf('/') + 1);
          //   console.log('filename  ' + filename);
          //   var formData: FormData = new FormData();
          //   formData.append('image', resFile, filename);
          //   this.http.post('localhost/uploadPic/46', formData)
          //     .subscribe(response => {console.log('response : ' + response)},
          //       error2 => {
          //         console.log('ERROR : ' + JSON.stringify(error2));
          //       });
          });
        });
      });



        // return File.readAsArrayBuffer(path, fileName);
//       File.readAsArrayBuffer(path, fileName).then((res)=>{
//         console.log('res ' + res);
// //never resolves.
//       }, (err) => {
//         console.log('error 1 : ', err);
//       });
      }, (err) => {
      console.log('error 2 : ', err);
    });
  }

  newObne() {
    // let body: string = JSON.stringify({'image': this.formData});
    // // type: 'application/json',
    // let headers: any = new Headers({'Content-Type': 'multipart/form-data'}),
    //   options: any = new RequestOptions({ headers: headers }),
    //   url: any = 'localhost/uploadPic/46';
    //
    // this.http.post(url,body, options).map(this.extractData)
    //   .subscribe((data) => {
    //     console.log('data ' + data);
    //   }, (err) => {
    //   console.log('main error ' + err);
    //   });
  }


  // fileChanged(e: Event) {
  //
  //   var target: HTMLInputElement = e.target as HTMLInputElement;
  //   this.load = [];
  //   this.filesubmit = true;
  //   console.log('e target ' + e.target);
  //   // if (this.selectedFileType) {
  //     for (var i = 0; i < target.files.length; i++) {
  //       console.log('target : ' + JSON.stringify(target));
  //       let ext = target.files[i]['name'].substr((target.files[i]['name']).lastIndexOf('.') + 1);
  //       console.log('target.files[i] : ' + JSON.stringify(target.files[i]));
  //       console.log('target.files[i] : ' + target.files[i]);
  //       // this.imageURI = target.files[i];
  //       this.loadFile(target.files[i]);
  //       // if (this.selectedFileType === 'image') {
  //       //   this.image.find(item => item === ext.toLowerCase().trim()) !== undefined ? this.loadFile(target.files[i])
  //       //     : this.error();
  //       // } else if (this.selectedFileType === 'audio') {
  //       //   this.audio.find(item => item === ext.toLowerCase().trim()) !== undefined ? this.loadFile(target.files[i])
  //       //     : this.error();
  //       // } else if (this.selectedFileType === 'video') {
  //       //   this.video.find(item => item === ext.toLowerCase().trim()) !== undefined ? this.loadFile(target.files[i])
  //       //     : this.error();
  //       // }
  //     }
  //   // } else {
  //   //   this.filesubmit = false;
  //   //   this.msgs = [];
  //   //   this.msgs.push({
  //   //     severity: 'error', summary: 'Error Message',
  //   //     detail: 'Please Chose file'
  //   //   });
  //   // }
  // }
  // //
  // loadFile(img: File) {
  //   var formData: FormData = new FormData();
  //   formData.append('image', img, img.name);
  //
  //   console.log('img, img.name + ' + img +'    ' + img.name);
  //   // this.http.post('localhost/uploadPic/46', formData)
  //   //   .subscribe(response => {console.log('response : ' + response)},
  //   //   error2 => {
  //   //     console.log('ERROR : ' + JSON.stringify(error2));
  //   //   });
  //
  //   // this.msgs = [];
  //   // this.msgs.push({
  //   //   severity: 'info', summary: 'Info Message',
  //   //   detail: 'Click Submit If you want to Chose file Again'
  //   // });
  // }

  // private extractData(res: Response) {
  //   let body = res.json();
  //   // return just the response, or an empty array if there's no data
  //   return body || [];
  // }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  // uploadImage() {
  //   let postData = new FormData();
  //   postData.append('image', this.base64Image);
  //   let user_picture: userPicture = {} as any;
  //   user_picture.image = this.base64Image;
  //
  //   var headers = new Headers();
  //     headers.append('Access-Control-Allow-Origin', '*');
  //     headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
  //     headers.append('Accept', 'application/json');
  //     headers.append('content-type', 'application/json');
  //     let options = new RequestOptions({headers: headers});
  //
  //     console.log('user_picture ' + JSON.stringify(user_picture));
  //
  //   this.http.post('localhost/uploadPic/46', user_picture).subscribe(data => {
  //           console.log('succes ' + data);
  //           console.log('succes ' + JSON.stringify(data));
  //           localStorage.setItem('user_profile', JSON.stringify(data));
  //           // this.goto('U');
  //         },
  //         error => {
  //           this.presentToast(error);
  //         });
  // }
  //
  // uploadimage() {
  //   // var promise = new Promise((resolve, reject) => {
  //     this.filechooser.open().then((url) => {
  //       console.log('url : ' + url);
  //       this.imageURI = url;
  //       (<any>window).FilePath.resolveNativePath(url, (result) => {
  //         this.nativepath = result;
  //         console.log('this.nativepath : ' + this.nativepath);
  //         this.imageURI= this.nativepath;
  //         (<any>window).resolveLocalFileSystemURL(this.nativepath, (res) => {
  //           res.file((resFile) => {
  //             console.log('resFile : ' + JSON.stringify(resFile));
  //             let user_picture: userPicture = {} as any;
  //             user_picture.image = resFile.localURL;
  //             this.http.post('localhost/uploadPic/46', {'image' : resFile}).subscribe(data => {
  //                 console.log('succes ' + data);
  //                 console.log('succes ' + JSON.stringify(data));
  //                 localStorage.setItem('user_profile', JSON.stringify(data));
  //                 // this.goto('U');
  //               },
  //               error => {
  //               console.log('error ' + error);
  //                 this.presentToast(error);
  //               });
  //             // this.imgurl = resFile.localURL;
  //             var reader = new FileReader();
  //             reader.readAsArrayBuffer(resFile);
  //             reader.onloadend = (evt: any) => {
  //               console.log('evt : ' + JSON.stringify(evt));
  //               var imgBlob = new Blob([evt.target.result], { type: resFile.type });
  //               console.log('imgBlob : ' + JSON.stringify(imgBlob));
  //               // var imageStore = this.firestore.ref('/profileimages').child(firebase.auth().currentUser.uid);
  //               // imageStore.put(imgBlob).then((res) => {
  //               //   this.firestore.ref('/profileimages').child(firebase.auth().currentUser.uid).getDownloadURL().then((url) => {
  //               //     resolve(url);
  //               //   }).catch((err) => {
  //               //     reject(err);
  //               //   })
  //               // }).catch((err) => {
  //               //   reject(err);
  //               // })
  //             }
  //           })
  //         })
  //       })
  //     })
  //   // })
  //   // return promise;
  // }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: message,
      // subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  goto(sufix: any) {
    if(sufix === 'TU')
      this.navCtrl.push(TellUsPage);
    if(sufix === 'P')
      this.navCtrl.push(ProfilePage);
  }

}
