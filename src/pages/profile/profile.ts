import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TellUsPage } from '../tell-us/tell-us';
import { MatchPage } from '../match/match';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  goto(sufix: any) {
    if(sufix === 'M')
      this.navCtrl.push(MatchPage);
    if(sufix === 'P')
      this.navCtrl.push(ProfilePage);
    if(sufix === 'TU')
      this.navCtrl.push(TellUsPage);
  }

}
