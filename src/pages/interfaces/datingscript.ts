export class User {
  email: string;
  password: string;
  username: string;
  // repassword : string;
}

export class userPicture {
  image: any;
}

export class UserDetail {
  data_gender: string;
  data_birthday: string;
  data_location: string;
  data_location_state : string;
  data_location_city: string;
  data_location_lat: string;
  data_location_lng : string;
  data_body_type : string;
  data_height : string;
  data_eyes : string;
  data_hair : string;
  data_ethnicity : string;
  data_smoking: string;
  data_drinking: string;
  data_inmyownwords: string;
}

export class StaticArrays {
  static gender: any = [
    {id: 1, value: 'Man'}
    , {id: 2, value: 'Woman'}
  ];

  static months: any = [
    {id: 1, value: 'January'}
    , {id: 2, value: 'February'}
    , {id: 3, value: 'March'}
    , {id: 4, value: 'April'}
    , {id: 5, value: 'May'}
    , {id: 6, value: 'June'}
    , {id: 7, value: 'July'}
    , {id: 8, value: 'August'}
    , {id: 9, value: 'September'}
    , {id: 10, value: 'October'}
    , {id: 11, value: 'November'}
    , {id: 12, value: 'December'}
  ];

  static eyes: any = [
    {id: 1, value: 'Black'}
    , {id: 2, value: 'Blue'}
    , {id: 3, value: 'Brown'}
    , {id: 4, value: 'Gray'}
    , {id: 5, value: 'Green'}
    , {id: 6, value: 'Hazel'}
  ];

  static hair: any = [
    {id: 1, value: 'Auburn'}
    , {id: 2, value: 'Black'}
    , {id: 3, value: 'Blonde'}
    , {id: 4, value: 'Light brown'}
    , {id: 5, value: 'Dark brown'}
    , {id: 6, value: 'Red'}
    , {id: 7, value: 'White \/ gray'}
    , {id: 8, value: 'Bald'}
    , {id: 9, value: 'A little gray'}
  ];

  static height: any = [
    {id: 1, value: "5' 0\""}
    , {id: 2, value: "5' 1\""}
    , {id: 3, value: "5' 2\""}
    , {id: 4, value: "5' 3\""}
    , {id: 5, value: "5' 4\""}
    , {id: 6, value: "5' 5\""}
    , {id: 7, value: "5' 6\""}
    , {id: 8, value: "5' 7\""}
    , {id: 9, value: "5' 8\""}
    , {id: 10, value: "5' 9\""}
    , {id: 11, value: "5' 10\""}
    , {id: 12, value: "5' 11\""}
    , {id: 13, value: "6' 0\""}
    , {id: 14, value: "6' 1\""}
    , {id: 15, value: "6' 2\""}
    , {id: 16, value: "6' 3\""}
    , {id: 17, value: "6' 4\""}
    , {id: 18, value: "6' 5\""}
    , {id: 19, value: "6' 6\""}
  ];

  static body_type: any = [
    {id: 1, value: 'Slim'}
    , {id: 2, value: 'Slender'}
    , {id: 3, value: 'Average'}
    , {id: 4, value: 'Fit'}
    , {id: 5, value: 'Athletic'}
    , {id: 6, value: 'A few extra pounds'}
    , {id: 7, value: 'Large'}
    , {id: 8, value: 'Voluptuous'}
  ];

  static ethnicity: any = [
    {id: 1, value: 'African American (black)'}
    , {id: 2, value: 'Asian'}
    , {id: 3, value: 'Caucasian (white)'}
    , {id: 4, value: 'East indian'}
    , {id: 5, value: 'Hispanic \/ Latino'}
    , {id: 6, value: 'Middle Eastern'}
    , {id: 7, value: 'Native american'}
    , {id: 8, value: 'Pacific islander'}
    , {id: 9, value: 'Inter-racial'}
    , {id: 10, value: 'Other'}
  ];

  static smoking: any = [
    {id: 1, value: 'No'}
    , {id: 2, value: 'Socially'}
    , {id: 3, value: 'Daily'}
  ];

  static drinking: any = [
    {id: 1, value: 'No'}
    , {id: 2, value: 'Socially'}
    , {id: 3, value: 'Daily'}
  ];
}
