import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators, AbstractControl} from '@angular/forms';

import { TellUsPage } from '../tell-us/tell-us';
import { MatchPage } from '../match/match';
import { ProfilePage } from "../profile/profile";
import { TermsPrivacyPage } from '../terms-privacy/terms-privacy';
import { Http, Headers, RequestOptions } from '@angular/http';
import { EmailComposer } from '@ionic-native/email-composer';


// Interface
import { User } from "../interfaces/datingscript";
import 'rxjs/add/operator/map';

import * as firebase from 'firebase';
/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var cordova;

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  User = {} as User;
  repassword: string;
  verificationId : any;
  code: string = "";
  windowRef: any;
  verificationCode: any;
  // formgroup:FormGroup;
  // email:AbstractControl;
  // password:AbstractControl;
  // name:AbstractControl;
  // repassword:AbstractControl;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public formbuilder:FormBuilder,
              private http: Http,
              private emailComposer: EmailComposer) {

    // this.formgroup = formbuilder.group({
    //   email:['',Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
    //   password:['',Validators.compose([Validators.minLength(6), Validators.required])],
    //   repassword:['',Validators.compose([Validators.minLength(6), Validators.required])],
    //   name:['',Validators.compose([Validators.required,Validators.maxLength(15)])]
    // });
    //
    // this.email = this.formgroup.controls['email'];
    // this.password = this.formgroup.controls['password'];
    // this.repassword = this.formgroup.controls['repassword'];
    // this.name = this.formgroup.controls['name'];

    // this.presentAlert('hello');

  }

  ionViewDidLoad() {
    this.clearAll();
    console.log('ionViewDidLoad SignUpPage');
    // window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
  }

  clearAll() {
    this.User.email = this.User.password = this.repassword = this.User.username = '';
  }

  register() {
    console.log('SignUpPage');

    let phone = '+923153589993';

    console.log('LoginPage registerPhone() phone', phone);

    // cordova.plugins.firebase.auth.verifyPhoneNumber(phone).then(function(verificationId) {
    //   // pass verificationId to signInWithVerificationId
    // console.log('verificationId ' + verificationId);
    // }).catch((error) => {
    //   console.log('error ' + error);
    // });

    (<any>window).FirebasePlugin.getToken(function(token) {
      // save this server-side and use it to push notifications to this device
      console.log(token);
    }, function(error) {
      console.error(error);
    });

    (<any>window).FirebasePlugin.verifyPhoneNumber(phone, 60, (credential) => {
      console.log('credential ' + credential);

      // ask user to input verificationCode:
      // var code = inputField.value.toString();
      // var code = '102546';
      //
      // var verificationId = credential.verificationId;
      //
      // var credential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);
      //
      // // sign in with the credential
      // firebase.auth().signInWithCredential(credential);
      //
      // // call if credential.instantVerification was true (android only)
      // firebase.auth().signInWithCustomToken(customTokenFromYourServer);
      //
      // // OR link to an account
      // firebase.auth().currentUser.linkWithCredential(credential)
    }, function(error) {
      console.log('error ' + error);
      console.error(error);
    });

    // window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
    //   'size': 'invisible',
    //   'callback': function(response) {
    //     console.log('response ' + response);
    //     // reCAPTCHA solved, allow signInWithPhoneNumber.
    //     // onSignInSubmit();
    //   }
    // });
    //
    // var appVerifier = window.recaptchaVerifier;
    // firebase.auth().signInWithPhoneNumber(phone, appVerifier)
    //   .then(function (confirmationResult) {
    //     console.log('confirmationResult ' + confirmationResult);
    //     // SMS sent. Prompt user to type the code from the message, then sign the
    //     // user in with confirmationResult.confirm(code).
    //     window.confirmationResult = confirmationResult;
    //   }).catch(function (error) {
    //   // Error; SMS not sent
    //   // ...
    // });

    // var app = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    //
    //
    // firebase.auth().signInWithPhoneNumber(phone, app)
    //     .then(function (confirmationResult) {
    //       console.log(JSON.stringify((confirmationResult)));
    //       console.log('confirmationResult ' + confirmationResult);
    //       // SMS sent. Prompt user to type the code from the message, then sign the
    //       // user in with confirmationResult.confirm(code).
    //       // window.confirmationResult = confirmationResult;
    //     }).catch(function (error) {
    //     console.log(JSON.stringify((error)));
    //     // Error; SMS not sent
    //     // ...
    //   });

    //   .then((res) => {
    //   console.log('res ' + res);
    //   // firebase.auth().signInWithPhoneNumber(phone, appVerifier)
    //   //   .then(function (confirmationResult) {
    //   //     console.log(JSON.stringify((confirmationResult)));
    //   //     console.log('confirmationResult ' + confirmationResult);
    //   //     // SMS sent. Prompt user to type the code from the message, then sign the
    //   //     // user in with confirmationResult.confirm(code).
    //   //     // window.confirmationResult = confirmationResult;
    //   //   }).catch(function (error) {
    //   //   console.log(JSON.stringify((error)));
    //   //   // Error; SMS not sent
    //   //   // ...
    //   // });
    // }).catch(function (error) {
    //     console.log(JSON.stringify(error));
    //     // Error; SMS not sent
    //     // ...
    //   });


    // window.FirebasePlugin.getVerificationID(phone, id => {
    //   console.log("verificationID: " + id);
    //   this.verificationId = id;
    //   // this.showPrompt();
    // }, error => {
    //   console.log("error: " + JSON.stringify(error));
    // });

 //    var id = new firebase.auth.PhoneAuthProvider().verifyPhoneNumber("+923153589993", 60);
 // //      .then(function(verificationId) {
 // // console.log('sffd');
 // //    });
 //  console.log('id : ' + id);
 //    window.FirebasePlugin.verifyPhoneNumber(phone, 60, (credential) => {
 //      console.log('credential ' + credential);
 //
 //      // ask user to input verificationCode:
 //      // var code = inputField.value.toString();
 //
 //      this.verificationId = credential.verificationId;
 //
 //      // var signInCredential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);
 //      // firebase.auth().signInWithCredential(signInCredential);
 //    }, (error) => {
 //      console.error('error ' + error);
 //    });


    // let email = {
    //   to: 'shoaibhayat93@gmail.com',
    //   cc: ['shoaibhayat93@gmail.com','shoaibhayat62@gmail.com'],
    //   subject: 'les-bi',
    //   body: '02153',
    //   isHtml: true
    // };
    // this.emailComposer.open(email);

    // // console.log('email');
    // var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    //
    // if(this.User.email == '') {
    //   this.presentAlert('Email Address is required');
    //   return;
    // }
    //
    // if (!emailPattern.test(this.User.email)) {
    //   this.presentAlert('Invalid Email Address is required');
    //   return;
    //   return;
    // }
    //
    // if(this.User.password == '') {
    //   this.presentAlert('Password is required');
    //   return;
    // }
    //
    // if(this.User.password.length < 6) {
    //   this.presentAlert('Minimum 6 chracters is required');
    //   return;
    // }
    //
    // if(this.repassword == '') {
    //   t
    // his.presentAlert('Confirm is password required');
    //   return;
    // }
    //
    // if(this.repassword.length < 6) {
    //   this.presentAlert('Minimum 6 chracters is required');
    //   return;
    // }
    //
    // if(this.User.password != this.repassword) {
    //   this.presentAlert('Passwords does not match');
    //   return;
    // }
    //
    // if(this.User.username == '') {
    //   this.presentAlert('User Name is required');
    //   return;
    // }
    //
    // var headers = new Headers();
    // headers.append('Access-Control-Allow-Origin' , '*');
    // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    // headers.append('Accept','application/json');
    // headers.append('content-type','application/json');
    // let options = new RequestOptions({ headers:headers});
    //
    // this.http.post('localhost/usersignup/1/3', this.User, options).map(res => res.json()).subscribe(data => {
    //   console.log('succes ' + data);
    //     console.log('succes ' + JSON.stringify(data.data));
    //     localStorage.setItem('user', JSON.stringify(data.data));
    //     this.goto('TU');
    // },
    // error => {
    //   this.presentAlert(error);
    // });

    // this.goto('TU');
  }

  verify() {
    var signInCredential = firebase.auth.PhoneAuthProvider.credential(this.verificationId, this.code);
    firebase.auth().signInWithCredential(signInCredential).then((info) => {
      console.log('info :' + info);
    },
      (error) => {
        console.log('error :' + error);
    });
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: message,
      // subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  goto(sufix: any) {
    if(sufix === 'M')
      this.navCtrl.push(MatchPage);
    if(sufix === 'P')
      this.navCtrl.push(ProfilePage);
    if(sufix === 'TU')
      this.navCtrl.push(TellUsPage);
    if(sufix === 'TP')
      this.navCtrl.push(TermsPrivacyPage);
  }

}
