import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { SignUpPage } from '../sign-up/sign-up';
import { UploadPicturePage } from '../upload-picture/upload-picture';
import { Http, Headers, RequestOptions } from '@angular/http';

// interfaces
import { StaticArrays } from '../interfaces/datingscript'
import { User, UserDetail } from "../interfaces/datingscript";

import * as $ from "jquery";
/**
 * Generated class for the TellUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tell-us',
  templateUrl: 'tell-us.html',
})
export class TellUsPage implements OnInit {

  eyes      : any;
  hairs     : any;
  drinking  : any;
  smoking   : any;
  ethnicity : any;
  body_type : any;
  height    : any;
  gender    : any;
  months    : any;
  countries : any;
  cities    : any;
  states    : any;
  days      : any  = [];
  years     : any  = [];

  eye       : any  = '';
  hair      : any  = '';
  drink     : any  = '';
  smoke     : any  = '';
  ethnic    : any  = '';
  body_t    : any  = '';
  heigh     : any  = '';
  gend      : any  = '';
  country   : any  = '';
  city      : any  = '';
  state     : any  = '';
  year      : any  = '';
  month     : any  = '';
  day       : any  = '';
  inmyownwords:any = '';
  userID    : any  = '';

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              private http: Http)
  { }

  ngOnInit() {
    // this.userID = '';
    // if(localStorage.getItem('user')) {
    //   let user = JSON.parse(localStorage.getItem('user'));
    //   this.userID = user.user_id;
    // }
    // else {
    //   this.goto('S');
    // }

    this.eyes = StaticArrays.eyes;
    this.hairs = StaticArrays.hair;
    this.smoking = StaticArrays.smoking;
    this.drinking = StaticArrays.drinking;
    this.ethnicity = StaticArrays.ethnicity;
    this.height = StaticArrays.height;
    this.body_type = StaticArrays.body_type;
    this.gender = StaticArrays.gender;
    this.months = StaticArrays.months;
    var year = 1970;
    var lstyear = new Date().getFullYear();
    var yearDiff = (lstyear - year) + 1;
    console.log(lstyear + 'lst ');
    for(let i = 0; i < yearDiff; i++){
      this.years.push(lstyear);
      lstyear = lstyear-1;
    }

    this.http.get('localhost/get_country').map(res => res.json()).subscribe(data => {
      console.log('countries ' + JSON.stringify(data.data));
      this.countries = data.data;
    },
    error => {
      this.presentAlert(error);
    });
  }

  ionViewDidLoad() {
    this.clearAll();

    // $(document).ready(function () {
    //   var year = 1920;
    //   var lstyear = new Date().getFullYear();
    //   var yearDiff = (lstyear - year) + 1;
    //   console.log(lstyear + 'lst ');
    //   for(let i = 0; i < yearDiff; i++){
    //     // $("#years").get(0).options[$("#years").get(0).options.length] = new Option(lstyear, lstyear);
    //     // lstyear=lstyear-1;
    //   }
    // });
    // console.log(this.year);

    // console.log('StaticArrays.eyes' + JSON.stringify(StaticArrays.eyes));
    // for(let item = 0; item < StaticArrays.eyes.length; item++) {
    //     console.log(StaticArrays.eyes[item]);
    //   console.log(StaticArrays.eyes[item].id);
    // }
    console.log('ionViewDidLoad TellUsPage');
  }


  clearAll() {
    this.eye =  this.hair  =  this.drink =  this.smoke  =  this.ethnic
      = this.body_t  = this.heigh = this.gend = this.day = this.month = this.year = '';
    this.days = [];
  }

  goto(sufix: any) {
    if(sufix === 'S')
      this.navCtrl.push(SignUpPage);
    if(sufix === 'U')
      this.navCtrl.push(UploadPicturePage);
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: message,
      // subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  pick() {
    if(this.year === '') {
      this.presentAlert('Please select year first');
    }
    if(this.month === '') {
      this.presentAlert('Please select month first');
    }
  }

  onChange(event , sufix) {
    // About
    if(sufix === 'G') {
      // if(event != '') {
        this.gend = event;
        console.log('sufix ' + sufix + ' gender ' + this.gend);
      // }
    }
    else if(sufix === 'C') {
      // if(event != '') {
      this.country = event;
      console.log('sufix ' + sufix + ' country ' + this.country);
      this.http.get('localhost/get_state/' + this.country).map(res => res.json()).subscribe(data => {
          console.log('states ' + JSON.stringify(data.data));
          this.states = data.data;
        },
        error => {
          this.presentAlert(error);
        });
      // }
    }
    else if(sufix === 'CS') {
      // if(event != '') {
      this.state = event;
      console.log('sufix ' + sufix + ' state ' + this.state);
      this.http.get('localhost/get_city/' + this.state).map(res => res.json()).subscribe(data => {
          console.log('cities ' + JSON.stringify(data.data));
          this.cities = data.data;
        },
        error => {
          this.presentAlert(error);
        });
      // }
    }
    else if(sufix === 'CC') {
      // if(event != '') {
      this.city = event;
      console.log('sufix ' + sufix + ' city ' + this.city);
      //}
    }
    else if(sufix === 'DY') {
      // if(event != '') {
      this.day = event;
      console.log('sufix ' + sufix + ' day ' + this.day);
      // }
    }
    else if(sufix === 'M') {
      // if(event != '') {
      this.month = event;
      this.days = [];
      console.log('sufix ' + sufix + ' month ' + this.month);
      if(this.year !== '') {
        let days = new Date(this.year, this.month, 0).getDate();
        for(let i = 1; i <= days; i++){
          this.days.push(i);
        }
        console.log(JSON.stringify(this.days));
      }
      // }
    }
    else if(sufix === 'Y') {
      // if(event != '') {
      this.year = event;
      this.days = [];
      console.log('sufix ' + sufix + ' year ' + this.year);
      if(this.month !== '') {
        let days = new Date(this.year, this.month, 0).getDate();
        for(let i = 1; i <= days; i++){
          this.days.push(i);
        }
        console.log(JSON.stringify(this.days));
      }
      // }
    }


    //  Appearance
    else if(sufix === 'E') {
      // if(event != '') {
        this.eye = event;
        console.log('sufix ' + sufix + ' eye ' + this.eye);
      // }
    }
    else if(sufix === 'H') {
      // if(event != '') {
        this.hair = event;
        console.log('sufix ' + sufix + ' hair ' + this.hair);
      // }
    }
    else if(sufix === 'HG') {
      // if(event != '') {
        this.heigh = event;
        console.log('sufix ' + sufix + ' height ' + this.heigh);
      // }
    }
    else if(sufix === 'ET') {
      // if(event != '') {
        this.ethnic = event;
        console.log('sufix ' + sufix + ' ethnicity ' + this.ethnic);
      // }
    }
    else if(sufix === 'B') {
      // if(event != '') {
        this.body_t = event;
        console.log('sufix ' + sufix + ' body_type ' + this.body_t);
      // }
    }

    //  Lifestyle
    else if(sufix === 'S') {
      // if(event != '') {
        this.smoke = event;
        console.log('sufix ' + sufix + ' smoking ' + this.smoke);
      // }
    }
    else if(sufix === 'D') {
      // if(event != '') {
        this.drink = event;
        console.log('sufix ' + sufix + ' drinking ' + this.drink);
      // }
    }
  }

  continue() {
    // if (this.userID !== '') {
    //   // About
    //   if (this.gend === '') {
    //     this.presentAlert('Please select gender');
    //     return;
    //   }
    //   if (this.year === '') {
    //     this.presentAlert('Please select year');
    //     return;
    //   }
    //   if (this.month === '') {
    //     this.presentAlert('Please select month');
    //     return;
    //   }
    //   if (this.day === '') {
    //     this.presentAlert('Please select date');
    //     return;
    //   }
    //   if (this.country === '') {
    //     this.presentAlert('Please select country');
    //     return;
    //   }
    //   if (this.state === '') {
    //     this.presentAlert('Please select state');
    //     return;
    //   }
    //   if (this.city === '') {
    //     this.presentAlert('Please select city');
    //     return;
    //   }
    //
    //   // Appearance
    //   if (this.body_t === '') {
    //     this.presentAlert('Please select body type');
    //     return;
    //   }
    //   if (this.heigh === '') {
    //     this.presentAlert('Please select height');
    //     return;
    //   }
    //   if (this.eye === '') {
    //     this.presentAlert('Please select eye');
    //     return;
    //   }
    //   if (this.hair === '') {
    //     this.presentAlert('Please select hair');
    //     return;
    //   }
    //   if (this.ethnic === '') {
    //     this.presentAlert('Please select ethnicity');
    //     return;
    //   }
    //
    //   //  Lifestyle
    //   if (this.smoke === '') {
    //     this.presentAlert('Please select smoking');
    //     return;
    //   }
    //   if (this.drink === '') {
    //     this.presentAlert('Please select drinking');
    //     return;
    //   }
    //
    //   let birthDate = new Date(this.year, this.month, this.day);
    //   var ageDifMs = Date.now() - birthDate.getTime();
    //   var ageDate = new Date(ageDifMs); // miliseconds from epoch
    //   var birthYears = Math.abs(ageDate.getUTCFullYear() - 1970);
    //   console.log('birthYears ' + birthYears);
    //   if (birthYears < 17) {
    //     this.presentAlert('You are under age! Must be minimum 18 years old');
    //     return;
    //   }
    //   if(this.month < 10) {
    //     this.month = this.month.replace(/^0+/, '');
    //     console.log('this.month ' + this.month);
    //     this.month = '0' + this.month;
    //   }
    //   if(this.day < 10) {
    //     this.day = this.day.replace(/^0+/, '');
    //     console.log('this.day ' + this.day);
    //     this.day = '0' + this.day;
    //   }
    //
    //   console.log('Details :--- ' + this.gend + ' , ' + this.year + this.month + this.day + ' , ' + this.country
    //     + ' , ' + this.state + ' , ' + this.city + ' Appearance ' + this.body_t + ' , ' +
    //     this.heigh + ' , ' + this.hair + ' , ' + this.eye + ' , ' +
    //     this.ethnic + ' Lifestyle ' + this.smoke + ' , ' + this.drink + ' , ' + this.inmyownwords);
    //
    //   let user_detail: UserDetail = {} as any;
    //   //  About
    //   user_detail.data_gender = this.gend;
    //   user_detail.data_birthday = this.year + this.month + this.day;
    //   user_detail.data_location = this.country;
    //   user_detail.data_location_state = this.state;
    //   user_detail.data_location_city = this.city;
    //   user_detail.data_location_lat = '0.00';
    //   user_detail.data_location_lng = '0.00';
    //   // Appearance
    //   user_detail.data_ethnicity = this.ethnic;
    //   user_detail.data_eyes = this.eye;
    //   user_detail.data_hair = this.hair;
    //   user_detail.data_height = this.heigh;
    //   user_detail.data_body_type = this.body_t;
    //   //  Lifestyle
    //   user_detail.data_smoking = this.smoke;
    //   user_detail.data_drinking = this.drink;
    //   user_detail.data_inmyownwords = this.inmyownwords;
    //
    //   var headers = new Headers();
    //   headers.append('Access-Control-Allow-Origin', '*');
    //   headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    //   headers.append('Accept', 'application/json');
    //   headers.append('content-type', 'application/json');
    //   let options = new RequestOptions({headers: headers});
    //
    //   this.http.post('localhost/profiledetail/' + this.userID, user_detail, options).map(res => res.json()).subscribe(data => {
    //       console.log('succes ' + data);
    //       console.log('succes ' + JSON.stringify(data.data));
    //       localStorage.setItem('user_profile', JSON.stringify(data.data));
    //       this.goto('U');
    //     },
    //     error => {
    //       this.presentAlert(error);
    //     });
    // }
    // else {
    //   this.presentAlert('User Id not found');
    //   this.goto('S');
    // }

    this.goto('U');
  }

}
