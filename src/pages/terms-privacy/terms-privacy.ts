import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SignUpPage } from '../sign-up/sign-up';
/**
 * Generated class for the TermsPrivacyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-terms-privacy',
  templateUrl: 'terms-privacy.html',
})
export class TermsPrivacyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsPrivacyPage');
  }

  goto(sufix: any) {
    if(sufix === 'S')
      this.navCtrl.push(SignUpPage);
  }

}
