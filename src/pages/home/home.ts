import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ProfilePage } from '../profile/profile';
import { SignUpPage } from '../sign-up/sign-up';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goto(sufix: any) {
    if(sufix === 'S')
      this.navCtrl.push(SignUpPage);
    if(sufix === 'P')
      this.navCtrl.push(ProfilePage);
  }

}
