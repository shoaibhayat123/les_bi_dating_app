import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { AlbumPage } from '../pages/album/album';
import { MatchPage } from '../pages/match/match';
import { ProfilePage } from '../pages/profile/profile';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { TellUsPage } from '../pages/tell-us/tell-us';
import { TermsPrivacyPage } from '../pages/terms-privacy/terms-privacy';
import { UploadPicturePage } from '../pages/upload-picture/upload-picture';

import { FileChooser } from '@ionic-native/file-chooser';
import { HttpModule } from '@angular/http';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { EmailComposer } from '@ionic-native/email-composer';

import * as firebase from 'firebase';

firebase.initializeApp({
  apiKey: "AIzaSyAv-osOkU2zSsLC3WVn04rH2yV4YuSZro8",
  authDomain: "smsverification-ddf61.firebaseapp.com",
  databaseURL: "https://smsverification-ddf61.firebaseio.com",
  projectId: "smsverification-ddf61",
  storageBucket: "smsverification-ddf61.appspot.com",
  messagingSenderId: "797847268458"
});

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignUpPage,
    AlbumPage,
    MatchPage,
    ProfilePage,
    TellUsPage,
    TermsPrivacyPage,
    UploadPicturePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignUpPage,
    AlbumPage,
    MatchPage,
    ProfilePage,
    TellUsPage,
    TermsPrivacyPage,
    UploadPicturePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FileChooser,
    FileTransfer,
    Camera,
    EmailComposer,
    // FileUploadOptions,
    // FileTransferObject,
    File,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
